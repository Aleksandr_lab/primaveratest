<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AminsRoutes extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /**@test */
    public function can_get_admin_main_page()
    {
        $response = $this->get('/admin');

        $response->assertStatus(200);
    }

    /**@test */
    public function can_get_admin_blog_page()
    {
        $response = $this->get('/admin/blog');

        $response->assertStatus(200);
    }

    /**@test */
    public function can_get_admin_portfolio_page()
    {
        $response = $this->get('/admin/portfolio');

        $response->assertStatus(200);
    }

    /**@test */
    public function can_get_admin_services_page()
    {
        $response = $this->get('/admin/services');

        $response->assertStatus(200);
    }

    /**@test */
    public function can_get_admin_users_page()
    {
        $response = $this->get('/admin/user');

        $response->assertStatus(200);
    }
}
