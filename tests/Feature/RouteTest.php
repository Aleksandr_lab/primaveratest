<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RouteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /**
        @test
     */
    public function can_get_home_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
        @test
     */
    public function can_get_blog_page()
    {
        $response = $this->get('/blog');

        $response->assertStatus(200);
    }

    /**
        @test
     */
    public function can_get_services_page()
    {
        $response = $this->get('/services');

        $response->assertStatus(200);
    }

    /**
        @test
     */
    public function can_get_portfolio_page()
    {
        $response = $this->get('/portfolio');

        $response->assertStatus(200);
    }
}
