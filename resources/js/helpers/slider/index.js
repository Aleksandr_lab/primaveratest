export const  gallerySingelPost = () => {
    const gallery = document.querySelector(".gallery");
    if(gallery){
        const listOfImages = gallery.querySelectorAll("img");
        /*Constructor Gallery*/
        const funcCreateElem = (className='', appendElem=null, nameElem='div') => {
            const elem = document.createElement(nameElem);
            elem.className = className;
            if(appendElem) {
                return appendElem.appendChild(elem)
            }
            return elem;
        };

        /*Gallery Counter*/
        const galleryCounter = funcCreateElem('gallery__counter', gallery),
            galleryCounterCurrent = funcCreateElem('gallery__counter-current', galleryCounter, 'span'),
            galleryCounterAll = funcCreateElem('gallery__counter-all', galleryCounter, 'span');
        galleryCounterCurrent.innerHTML = '1';
        galleryCounterAll.innerHTML = '&ndash;' + listOfImages.length;

        /*Current Image*/
        const currentImageBox = funcCreateElem('gallery__current-image-box', gallery),
            currentImage = funcCreateElem('gallery__current-image', currentImageBox, 'img');
        currentImage.classList.add('gallery__current-image');
        currentImage.src = listOfImages[0].src;
        listOfImages[0].classList.add('gallery__current-image-list');
        /*Gallery action*/
        const galleryAction = funcCreateElem('gallery__action', currentImageBox),
            btnPrev = funcCreateElem('gallery__btn gallery__btn-prev', galleryAction, 'div'),
            btnNext = funcCreateElem('gallery__btn gallery__btn-next', galleryAction, 'div');

        const galleryConteiner = funcCreateElem('gallery__conteiner', gallery),
            galleryItem = [];
        listOfImages.forEach((el, i)=>{
            el.classList.add('gallery__item-image');
            galleryItem.push(funcCreateElem('gallery__item'));
            galleryItem[i].appendChild(el);
            galleryConteiner.appendChild(galleryItem[i]);
        });

        listOfImages.forEach((el, i)=>el.setAttribute('data-index', i+1));

        const styleList = () => {
                if (galleryConteiner.scrollWidth === galleryConteiner.offsetWidth) {
                    galleryConteiner.style.justifyContent = "center";
                } else {
                    galleryConteiner.style.justifyContent = "flex-start";
                }
            },

            goToRight = () => {
                let current = galleryConteiner.querySelector(".gallery__current-image-list");
                if(current.parentElement.nextElementSibling){
                    current.parentElement.nextElementSibling.children[0].classList.add("gallery__current-image-list");
                }else{
                    listOfImages[0].classList.add("gallery__current-image-list");
                }

                current.classList.remove("gallery__current-image-list");
                current = galleryConteiner.querySelector(".gallery__current-image-list");
                galleryConteiner.scrollLeft = current.offsetLeft;
                currentImage.src = current.src;
                galleryCounterCurrent.innerHTML = current.dataset.index;
                currentImage.classList.add("slideInFromRight");
                setTimeout(function() {
                    currentImage.classList.remove("slideInFromRight");
                }, 500);
            },

            goToLeft = () => {
                let current = galleryConteiner.querySelector(".gallery__current-image-list");
                if(current.parentElement.previousElementSibling){
                    current.parentElement.previousElementSibling.children[0].classList.add("gallery__current-image-list");
                }else{
                    listOfImages[listOfImages.length-1].classList.add("gallery__current-image-list");
                }
                current.classList.remove("gallery__current-image-list");
                current = galleryConteiner.querySelector(".gallery__current-image-list");
                galleryConteiner.scrollLeft = current.offsetLeft;
                currentImage.src = current.src;
                galleryCounterCurrent.innerHTML = current.dataset.index;
                currentImage.classList.add("slideInFromLeft");
                setTimeout(function() {
                    currentImage.classList.remove("slideInFromLeft");
                }, 500);
            },

            changeCurrentImage = function () {
                currentImage.classList.add("fadeIn");
                setTimeout(function() {
                    currentImage.classList.remove("fadeIn");
                }, 500);
                currentImage.src = this.src;
                galleryCounterCurrent.innerHTML = this.dataset.index;
                listOfImages.forEach(image => image.classList.remove("gallery__current-image-list"));
                this.classList.add("gallery__current-image-list");
            };

        styleList();

        btnPrev.addEventListener("click", goToLeft);
        btnNext.addEventListener("click", goToRight);

        window.addEventListener("resize", ()=> styleList());

        // (function() {
        //     if (typeof NodeList.prototype.forEach === "function") return false;
        //     NodeList.prototype.forEach = Array.prototype.forEach;
        // })();

        listOfImages.forEach(image => image.addEventListener("click", changeCurrentImage));
    }
};
