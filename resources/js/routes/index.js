import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routesData/allRoutes';
import store from '../store';

Vue.use(VueRouter);


/* Add Vue Router */

const router = new VueRouter({
  mode: 'history',
  routes,
});
router.beforeEach((to, from, next) => {
  if (!store.getters.loadingStatus) {
    next();
  }
});

const editTitle = (to) => {
  const title = document.querySelector('h1');
  if (to.path !== '/' && title) {
    document.title = title.innerHTML.replace(/<[^>]*>?/gm, '');
  } else {
    document.title = 'PRIMAVERA';
  }
};

router.afterEach((to, from) => {
  setTimeout(() => editTitle(to), 100);
  if (from.name) {
    store._mutations.togglePanelLeft[0](false);
    store._mutations.togglePanelRight[0](false);
    store._mutations.currentContent[0]('reset');
    window.scrollTo(0, 0);
  }
});

export default router;
