/*Component page*/
import Home from '../../components/pages/homePage/Home.vue';
import Services from '../../components/pages/servicesPage/Services.vue';
import About from '../../components/pages/aboutPage/About.vue';
import Blog from '../../components/pages/blogPage/Blog.vue';
import Gallery from '../../components/pages/galleryPage/Gallery.vue';
import Price from '../../components/pages/pricePage/Price.vue';
import Contact from '../../components/pages/contactPage/Contact';
import PhotoGallery from '../../components/pages/photoGalleryPage/PhotoGallery.vue';
import VideoGallery from '../../components/pages/videoGalleryPage/VideoGallery.vue';
import SingleBlog from  '../../components/pages/singleBlog/SingleBlog';
import SingleService from "../../components/pages/singleService/SingleService";
import SingleServiceSubCategory from "../../components/pages/singleServiceSubCategory/SingleServiceSubCategory";

export const routes = [
    {
        path: '/',
        name: 'home',
        menuName: 'Главная',
        component: Home,
    }, {
        path: '/services',
        name: 'services',
        menuName: 'Услуги',
        component: Services,
    },{
        path: '/about',
        name: 'about',
        menuName: 'О нас',
        component: About,
    }, {
        path: '/blog',
        name: 'blog',
        menuName: 'Блог',
        component: Blog,
    }, {
        path: '/gallery',
        name: 'gallery',
        menuName: 'Галерея',
        component: Gallery,
    }, {
        path: '/price',
        name: 'price',
        menuName: 'Прайс',
        component: Price
    },{
        path: '/contact',
        name: 'contact',
        menuName: 'Контакты',
        component: Contact
    }, {
        path: '/services/:slug',
        name: 'single-service',
        component: SingleService,
        notIndex: true,
    },{
        path: '/services/inside/:slug',
        name: 'single-service-sub-cutegory',
        component: SingleServiceSubCategory,
        notIndex: true,
    }, {
        path: '/blog/:slug',
        name: 'blog-category',
        component: Blog,
        notIndex: true,
    }, {
        path: '/blog/posts/:slug',
        name: 'single-blog',
        component: SingleBlog,
        notIndex: true,
    }, {
        path: '/gallery/photos',
        name: 'gallery-photos',
        menuName: 'Галерея фото',
        notIndex: true,
        component: PhotoGallery,
    }, {
        path: '/gallery/videos',
        name: 'gallery-videos',
        menuName: 'Галерея видео',
        notIndex: true,
        component: VideoGallery,
    },
];
