export const state = {
    searchServicesForm: null,
    regularMail: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    errorMessage :{
        required: 'Поле обязательное для заполнения',
        email: 'Ошибка при вводе электронной почты',
        phone: 'Введите коректный номер телефона',
        service: 'Пожалуйста выберите услугу',
        server: 'Произошла ошибка при отправке, пожалуйста попробуйте позже'
    },
    select: null,
    response: [],
    loadForm: false,
    currentServiceId:null,
};
state.required = e => {
    const req = e.target.querySelectorAll('form .required'),
    resp = {};
    resp.error = {};
    req.forEach(el=>{
        const input = el.querySelector('.form-field');
        if(!input.value){

            resp.error[input.name] = state.errorMessage.required;
            el.classList.add('invalid');
            resp.result = false;
        }else{
            el.classList.add('valid');
            resp.result = true;
        }
    });
    return resp;
};

state.validMail = (e, email) => {
    const resp = {};
    if(email){
        const itemMail = e.target.querySelector('.form__item-email');
        if( state.regularMail.test(email.toLowerCase())){
            itemMail.classList.add('valid');
            resp.result = true;
        }else{
            itemMail.classList.add('invalid');
            resp.error = {};
            resp.error =  state.errorMessage.email;
            resp.result =   false;
        }
    }
    return resp;
};


state.validPhone = (e, phone) => {
    const resp = {};
    if(phone.length !== 9){
        e.target.querySelector('.form__item-phone').classList.add('invalid');
        resp.error = state.errorMessage.phone;
        resp.result = false;
    }else {
        resp.result = true;
    }
    return resp;
};
