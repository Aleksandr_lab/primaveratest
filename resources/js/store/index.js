import Vue from 'vue';
import Vuex from 'vuex';

import basicSettings from './modules/ModulesBasicSettings';
import slidePanelLeft from './modules/ModuleSlidePanelLeft';
import slidePanelRight from './modules/ModuleSlidePanelRight';
import lazyLoad from './modules/ModulesLazyLoad';
import homePage from './modules/ModulesHomePage';
import forms from './modules/ModulesForms';
import modal from './modules/ModulesModal';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    basicSettings,
    lazyLoad,
    slidePanelLeft,
    slidePanelRight,
    forms,
    modal,
    homePage,
  },
});
