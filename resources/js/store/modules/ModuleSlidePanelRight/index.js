import state from '../../state/StateSlidePanelRight';

export default {
    mutations:{
        togglePanelRight(state, statusPanelRight){
            state.activePanelRight = statusPanelRight
        }
    },
    state,
    getters:{
        statusPanelRight(state){
            return state.activePanelRight;
        }
    },
};
