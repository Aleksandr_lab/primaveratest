import state from '../../state/StateModals';
export default {
    mutations:{
        statusModal(state,{name, status}){
            state.carrentModalClass = _.kebabCase(name);
            status?document.body.classList.add('activ-modal'):document.body.classList.remove('activ-modal');
            state.modal = status;
            let modal = state.carrentModal;
            for(let key in modal)modal[name?name:key]=status;
        },
        videoModalContent(state, video){
            state.videoModalCurrentVideo = video;
        },
    },
    state,
    getters:{
        getModalForm(state){
            return state.carrentModal.modalForm;
        },
        getModalJobForm(state){
            return state.carrentModal.modalJobForm;
        },
        modalVideoGallery(state){
            return state.carrentModal.modalVideoGallery;
        },
        getVideoModalContent(state){
            return state.videoModalCurrentVideo
        },
        getModal(state){
            return state.modal;
        },
        carrentModalClass(state){
            return state.carrentModalClass;
        }
    },
};
