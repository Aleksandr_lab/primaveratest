import { fetchBasicSettings } from '../../actions/ActionBasicSettings';
import state from '../../state/StateBasicSettings';

export default {
  actions: {
    fetchBasicSettings,
  },
  mutations: {
    addBasicContent(state, content) {
      state.basicContent.push(content);
    },
    loadingStatus(state, loading) {
      state.loading = loading;
    },
    funcWindowLoad(state, load) {
      let i = 0;
      const timerId = setInterval(() => {
        if (i >= 3) {
          clearTimeout(timerId);
          state.windowLoad = load;
        }
        i++;
      }, 1000);
      window.onload = () => state.windowLoad = load;
    },
    resizeW(state) {
      state.windowW = window.innerWidth;
    },
  },
  state,
  getters: {
    loadingStatus(state) {
      return state.loading;
    },
    respBasicContent(state) {
      return state.basicContent;
    },
    getMenu(state) {
      return state.mainMenu;
    },
    windowLoad(state) {
      return state.windowLoad;
    },
    windowW(state) {
      return state.windowW;
    },
  },
};
