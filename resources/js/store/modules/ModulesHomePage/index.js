import state from '../../state/StateHomePage';

export default {
    mutations:{
        allMagazines(state, magazines){
            state.magazines = magazines;
        },
        currentMagazine(state, id){
            state.magazine = id ? state.magazines.find(el=> el.id === id) : state.magazines[0];
        },
        handleMagazineArrow(state, id){
            if(id > state.magazines.length){
                state.magazine =  state.magazines[0];
            }else if(id <= 1){
                state.magazine =  state.magazines[state.magazines.length-1];
            }else{
                state.magazine = state.magazines.find(el=> el.id === id);
            }
        },
    },
    state,
    getters:{
        allMagazines(state){
            return state.magazines;
        },
        currenMagazine(state){
            return state.magazine;
        },
    },
};
