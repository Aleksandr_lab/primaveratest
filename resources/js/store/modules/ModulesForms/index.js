import {state} from '../../state/StateForms';
import {uploadSelect, sendForms} from '../../actions/ActionForms'

export default {
    actions:{
        sendForms,
        uploadSelect
    },
    mutations:{
        formResponse(state, response){
            _.isString(response) ? state.response.forEach((el, i)=> el.name === response ? state.response.splice(i,1):null): state.response.push(response);
        },
        sendFormLoad(state, loading){
            state.loadForm = loading;
        },
        getSelect(state, select){
            state.select = select;
        },
        responseSearchForm(state, res){
            state.searchServicesForm = res;
        },
        editCurrentServicesId(state, res){
            state.currentServiceId = res;
        },
    },
    state,
    getters:{
        getResultSearchForm(state){
            return state.searchServicesForm;
        },
        errorMessage(state){
            return state.errorMessage;
        },
        getFormLoad(state){
            return state.loadForm;
        },
        getResponse(state){
            return state.response;
        },
        getSelect(state){
            return state.select;
        },
        required(state){
            return state.required
        },
        validMail(state){
            return state.validMail;
        },
        validPhone(state){
            return state.validPhone;
        },
        currentServicesId(state){
            return state.currentServiceId;
        },
    },
};
