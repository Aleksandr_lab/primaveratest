import state from '../../state/StateSlidePanelLeft';

export default {
    mutations:{
        togglePanelLeft(state, statusPanelLeft){
            state.activePanel = statusPanelLeft
        }
    },
    state,
    getters:{
        statusPanelLeft(state){
            return state.activePanel;
        }
    },
};
