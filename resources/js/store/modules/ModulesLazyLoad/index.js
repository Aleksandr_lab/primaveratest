import {fetchLazyLoad} from "../../actions/ActionLazyLoad";
import state from '../../state/StateLazyLoad';

export default {
    actions:{
        fetchLazyLoad,
    },
    mutations:{
        currentContent(state, content){
            if(content !== 'reset'){
                state.lazeLoadApi.push(content);
            }else {
                state.lazeLoadApi = [];
            }
        },
        contentLoading(state, loading){
            state.loading = loading;
        },
    },
    state,
    getters:{
        getContent(state){
            return state.lazeLoadApi;
        },
        contentLoading(state){
            return state.loading;
        },
    },
};
