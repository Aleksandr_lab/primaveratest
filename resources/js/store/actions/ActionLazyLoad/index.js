import { fetchRequest } from '../../../helpers/fetch/fetchRequest';

export const fetchLazyLoad = (ctx, url) => {
  ctx.commit('contentLoading', true);
  fetchRequest({
    method: 'GET',
    url: `/api${url}`,
  })
    .then((res) => {
      ctx.commit('currentContent', res);
      ctx.commit('contentLoading', false);
    });
};
