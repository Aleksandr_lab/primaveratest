import {fetchRequest} from "../../../helpers/fetch/fetchRequest";

export const  fetchBasicSettings = (ctx, url) => {
        ctx.commit('loadingStatus', true);
        fetchRequest({
            method: 'GET',
            url:'/api' + (url)
        })
        .then(res => {
            ctx.commit('addBasicContent', res);
            ctx.commit('loadingStatus', false);
        });
};
