import {fetchRequest} from "../../../helpers/fetch/fetchRequest";
import axios from 'axios'

export const  sendForms = (ctx, {data, url, formName}) => {
        ctx.commit('sendFormLoad', true);
        axios({
            method: 'POST',
            data: data,
            url:url
        })
        .then(res => {
            ctx.commit('formResponse', {name: formName, status:true, message:'Форма успешно оправлена'});//res
        })
        .catch(error => {
            ctx.commit('formResponse',{name: formName, status:false, message:'Что то пошло не так попробуйде позже'} ) //error.message;
        })
        .finally(()=>{
            ctx.commit('sendFormLoad', false)
        });
};

export const  uploadSelect = (ctx, url) => {
    fetchRequest({
        method: 'GET',
        url:'/api' + (url)
    })
        .then(res => {
            ctx.commit('getSelect', res);
        });

};
