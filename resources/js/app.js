/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('es6-promise').polyfill();

import Vue from 'vue'
import App from './App'
import store from './store'
import router from './routes'

import {VueMasonryPlugin} from 'vue-masonry';
Vue.use(VueMasonryPlugin);

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);

import VueYoutube from 'vue-youtube'
Vue.use(VueYoutube);

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask);

const main = new Vue({
    el:'#app',
    store,
    router,
    render: h => h(App)
});



