<div id="app">
    <div id="content" class="site-content">
        <main id="site_main" class="main-content">
            @yield('content')
            <router-view></router-view>
        </main>
    </div>
</div>
