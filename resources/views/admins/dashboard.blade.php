@include('admins.parts.header')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            {{ Auth::user()->email }}
        </div>
        <div class="col-md-6">
            <form method="POST" action="/logout">@csrf<button>Logout</button></form>
        </div>
    </div>
    <div class="row">
        <dashboard :user="{{  Auth::user() }}"></dashboard>
    </div>
</div>
@include('admins.parts.footer')
