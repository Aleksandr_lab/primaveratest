<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/about', 'HomeController@getAboutSection')->name('mp_about');
Route::get('/our-services', 'HomeController@getServicesSection')->name('mp_services');
Route::get('/important', 'HomeController@getImportantSection')->name('mp_important');
Route::get('/team', 'HomeController@getTeamSection')->name('mp_team');
Route::get('/news', 'HomeController@getNewsSection')->name('mp_news');
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog/{id}', 'BlogController@get_post')->name('get_post');
Route::get('/blog/{cat_id}', 'BlogController@get_posts_by_categories')->name('get_posts_by_category');
Route::get('/services', 'ServiceController@index')->name('services');
Route::get('/services/{slug}', 'ServiceController@get_service')->name('get_servic');
Route::get('/services/inside/{slug}', 'ServiceController@get_service_sub_category')->name('get_service_sub_category');
Route::get('/services/order', 'ServiceController@get_services')->name('services-for-submit-form');
Route::get('/gallery/photos', 'GalleryController@get_photos')->name('get-photos');
Route::get('/gallery/videos', 'GalleryController@get_videos')->name('get-videos');
Route::get('/main-settings', 'HomeController@getMainSettings')->name('get-main-settings');
Route::get('/home-banner', 'HomeController@getBannerSection')->name('get-banner-section');
Route::get('/home-partners', 'HomeController@getPartnersSection')->name('get-partner-section');


