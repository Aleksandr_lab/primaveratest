<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

### Auth ###
Auth::routes(['register' => false]);
### Admins ###

Route::get('/admin', 'Admins\AdminController@index')->name('admin');
Route::resources(['/admin/users' => 'Admins\UsersController']);
Route::resources(['/admin/posts' => 'Admins\PostController']);
Route::resources(['/admin/services' => 'Admins\ServiceController']);
Route::resources(['/admin/service-categories' => 'Admins\ServiceCategoriesController']);
Route::resources(['/admin/service-subcategories' => 'Admins\ServiceSubCategoriesController']);
Route::resources(['/admin/photo-gallery' => 'Admins\PhotoGalleryController']);
Route::resources(['/admin/video-gallery' => 'Admins\VideoGalleryController']);
Route::resources(['/admin/orders' => 'Admins\OrderController']);
Route::get('/admin/main-settings', 'Admins\MainSettingsController@getAllSettings');
Route::put('/admin/main-settings/{id}','Admins\MainSettingsController@updateSetting');
Route::delete('/admin/main-settings/{id}','Admins\MainSettingsController@deleteSetting');
Route::post('/admin/main-settings/create-contact','Admins\MainSettingsController@createContact');
Route::resources(['/admin/main-page-settings' => 'Admins\MainPageSettingsController']);
Route::resources(['/admin/about-page-settings' => 'Admins\AboutPageSettingsController']);
Route::get('/admin/subscribers', 'Admins\ClientsController@getSubscribers')->name('admin-subscribers');
Route::get('/admin/submitters', 'Admins\ClientsController@getSubmitters')->name('admin-submitters');
Route::resources(['/admin/employers' => 'Admins\EmployersController']);
Route::resources(['/admin/partners' => 'Admins\PartnersController']);

//### Guest ###
/*Route::get('/{any}', 'HomeController@index')
    ->where('any', '.*');*/

// Need to refactor this routes //
Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@index');
Route::get('/our-services', 'HomeController@index');
Route::get('/team', 'HomeController@index');
Route::get('/home-partners', 'HomeController@index');
Route::get('/news', 'HomeController@index');
Route::get('/blog', 'HomeController@index');
Route::get('/blog/posts/{id}', 'HomeController@index');
Route::get('/blog/{cat_id}', 'HomeController@index');
Route::get('/services', 'HomeController@index');
Route::get('/services/{slug}', 'HomeController@index');
Route::get('/services/inside/{slug}', 'HomeController@index');
//Route::get('/services/order', 'HomeController@index');  ????????
Route::get('/gallery', 'HomeController@index');
Route::get('/gallery/photos', 'HomeController@index');
Route::get('/gallery/videos', 'HomeController@index');
Route::get('/price', 'HomeController@index');
Route::get('/contact', 'HomeController@index');


Route::post('/add-subscriber','FormController@addSubscriber')->name('mp_addSubscriber');
Route::post('/add-contact-request','FormController@addContactRequest')->name('mp_addContactRequests');
Route::post('/order', 'OrderController@store')->name('order-create');
Route::post('/order/send_mail', 'OrderController@send_mail')->name('send_mail');







