<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Post;
use App\Models\PostsCategory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $post_categories = PostsCategory::pluck('id')->toArray();
    $title = $faker->sentence(3);
    $subTitle = $faker->sentence(3);
    $slug = Str::slug($title, '-');

    return [
        'title' => $title,
        'sub_title' => $subTitle,
        'slug' => $slug,
        'image_link' =>$faker->imageUrl($width=640, $height=480, 'cats'),
        'body' => $faker->realText(300),
        'category_id' => $faker->randomElement($post_categories)
    ];
});
