<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Service;
use Faker\Generator as Faker;
use App\Models\ServiceSubCategory;
use Illuminate\Support\Str;

$factory->define(Service::class, function (Faker $faker) {

    $services_sub_categories = ServiceSubCategory::pluck('id')->toArray();
    $title = $faker->sentence(3);
    $slug = Str::slug($title, '-');

    return [
        'title' => $title,
        'slug' => $slug,
        'price' => $faker->numberBetween($min = 500, $max = 2000),
        'sort_order' => $faker->numberBetween($min = 1, $max = 92),
        'body' => $faker->realText(300),
        'subcategory_id' => $faker->randomElement($services_sub_categories)
    ];
});
