<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ServiceSubCategory;
use Faker\Generator as Faker;
use App\Models\ServiceCategory;
use Illuminate\Support\Str;
$factory->define(ServiceSubCategory::class, function (Faker $faker) {

    $services_categories = ServiceCategory::pluck('id')->toArray();
    $title = $faker->sentence(3);
    $slug = Str::slug($title, '-');
    return [
        'title' => $title,
        'slug' => $slug,
        'sort_order' => $faker->numberBetween($min = 1, $max = 10),
        'category_id' =>$faker->randomElement($services_categories),

    ];
});
