<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Photo;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {
    $catImage = rand(0,1);
    $catImage = $catImage?'nature':'cats';
    return [
        'alt' => $faker->sentence(2),
        'image_link' => $faker->imageUrl($width=800, $height=600, $catImage)
    ];
});
