<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $users;

    public function __construct()
    {
        $this->users = [
            [
                'name' => 'admin',
                'email' => 'wizzard.zp@gmail.com',
                'password' => bcrypt('subrosa112358'),
                'role' => 'admin'
            ],
            [
                'name' => 'editor',
                'email' => 'serdyukartyom.zp@gmail.com',
                'password' => bcrypt('subrosa112358'),
                'role' => 'editor'
            ]
        ];
    }

    public function run()
    {
        DB::table('users')->insert($this->users);
    }
}
