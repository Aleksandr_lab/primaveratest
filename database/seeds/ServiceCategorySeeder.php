<?php

use Illuminate\Database\Seeder;

class ServiceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $services_categories = [
        [
            'title' => 'Косметология лица',
            'slug' => 'facial-cosmetology',
            'sort_order' => 1,
            'image' => '/resources/img/serviceCategory/face-cosmetology.png'
        ],
        [
            'title' => 'Косметология тела',
            'slug' => 'body-cosmetology',
            'sort_order' => 2,
            'image' => '/resources/img/serviceCategory/body-cosmetology.png'
        ],
        [
            'title' => 'Аппаратная косметология',
            'slug' => 'hardware-cosmetology',
            'sort_order' => 3,
            'image' => '/resources/img/serviceCategory/hairstyle.jpeg'
        ],
        [
            'title' => 'Уход за волосами',
            'slug' => 'hair-care',
            'sort_order' => 4,
            'image' => '/resources/img/serviceCategory/hair_care.png'
        ],
        [
            'title' => 'Мужской зал',
            'slug' => 'mens-room',
            'sort_order' => 5,
            'image' => '/resources/img/serviceCategory/mens.png'
        ],
        [
            'title' => 'Ногтевой сервис',
            'slug' => 'manicure',
            'sort_order' => 6,
            'image' => '/resources/img/serviceCategory/hairstyle.jpeg'
        ],
        [
            'title' => 'Имиждж сервис',
            'slug' => 'image-service',
            'sort_order' => 7,
            'image' => '/resources/img/serviceCategory/image_services.png'
        ],
        [
            'title' => 'Экспресс услуги',
            'slug' => 'express-services',
            'sort_order' => 8,
            'image' => '/resources/img/serviceCategory/express_services.png'
        ]
    ];
    public function run()
    {
        DB::table('service_categories')->insert($this->services_categories);
    }
}
