<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeesder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $permissions = [
        [
            'slug' => 'edit-blog',
            'name' => 'edit blog'
        ],
        [
            'slug' => 'edit-services',
            'name' => 'edit services'
        ],
        [
            'slug' => 'edit-portfolio',
            'name' => 'edit portfolio'
        ],
        [
            'slug' => 'edit-users',
            'name' => 'edit users'
        ]
    ];

    public function run()
    {
        DB::table('permissions')->insert($this->permissions);
    }
}
