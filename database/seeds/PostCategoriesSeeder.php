<?php

use Illuminate\Database\Seeder;

class PostCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $post_categories = [
        [
            'name' => 'НОВОСТИ',
            'slug' => 'news'
        ],
        [
            'name' => 'СТАТЬИ',
            'slug' => 'posts'
        ],
        [
            'name' => 'СОБЫТИЯ',
            'slug' => 'events'
        ],
    ];
    public function run()
    {
        DB::table('post_categories')->insert($this->post_categories);
    }
}
