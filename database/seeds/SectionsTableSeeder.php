<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $sections = [
        [
            'title' => 'Banner',
            'sub_title' => null,
            'slug' => 'banner',
            'page_id' => 1,
            'text' => null
        ],
        [
            'title' => 'О нас',
            'sub_title' => 'О нас',
            'slug' => 'about',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Наши услуги',
            'sub_title' => 'Наши Услуги',
            'slug' => 'services',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'О Важном',
            'sub_title' => 'О Важном',
            'slug' => 'important',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Наша команда',
            'sub_title' => 'Наша команда',
            'slug' => 'team',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Beauty News',
            'sub_title' => 'Beauty News',
            'slug' => 'news',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Наши Партнеры',
            'sub_title' => 'Наши Партнеры',
            'slug' => 'partners',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'AbBanner',
            'sub_title' => null,
            'slug' => 'abBanner',
            'page_id' => 2,
            'text' => null
        ],
        [
            'title' => 'abMain',
            'sub_title' => 'abMain',
            'slug' => 'abMain',
            'page_id' => 2,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'abProfessionals',
            'sub_title' => 'abProfessionals',
            'slug' => 'abProfessionals',
            'page_id' => 2,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'О Важном',
            'sub_title' => 'О Важном',
            'slug' => 'abImportant',
            'page_id' => 2,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Наша команда',
            'sub_title' => 'Наша команда',
            'slug' => 'abTeam',
            'page_id' => 2,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Beauty News',
            'sub_title' => 'Beauty News',
            'slug' => 'abNews',
            'page_id' => 2,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'Bаш стиль в надежных руках',
            'sub_title' => 'Bаш стиль в надежных руках',
            'slug' => 'style',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'title' => 'blockquote',
            'sub_title' => 'blockquote',
            'slug' => 'blockquote',
            'page_id' => 1,
            'text' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ]
    ];
    public function run()
    {
        DB::table('sections')->insert($this->sections);
    }
}
