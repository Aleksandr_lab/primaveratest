<?php

use Illuminate\Database\Seeder;
use App\Models\MainPageSetting;
class MainPageSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $settings = [
        [
            'name' => 'О нас',
            'sub_name' => 'О нас',
            'slug' => 'about',
            'value' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'name' => 'Наши услуги',
            'sub_name' => 'Наши Услуги',
            'slug' => 'our_services',
            'value' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'name' => 'О Важном',
            'sub_name' => 'О Важном',
            'slug' => 'about_important',
            'value' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ],
        [
            'name' => 'Наша команда',
            'sub_name' => 'Наша команда',
            'slug' => 'our_team',
            'value' => 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem IpsumLorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem IpsumLorem Ipsum',
        ]
    ];
    public function run()
    {
        DB::table('main_page_settings')->insert($this->settings);
    }
}
