<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $pages = [
        [
            'name' => 'main',
            'slug' => 'main',
        ],
        [
            'name' => 'about us',
            'slug' => 'aboutUs',
        ]
    ];
    public function run()
    {
        DB::table('pages')->insert($this->pages);
    }
}
