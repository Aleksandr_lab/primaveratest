<?php

use Illuminate\Database\Seeder;
use App\Models\ServiceSubCategory;

class ServiceSubCategoriesTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        factory(ServiceSubCategory::class, 10)->create();
    }
}
