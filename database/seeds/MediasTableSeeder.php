<?php

use Illuminate\Database\Seeder;

class MediasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $medias = [
        [
            'type' => 'video',
            'value' => 'https://www.youtube.com/embed/-337c2a3s0A?rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2F127.0.0.1%3A8000&amp;widgetid=3'
        ],
        [
            'type' => 'img',
            'value' => 'bg_banner_home.jpg'
        ],
        [
            'type' => 'img',
            'value' => 'bg_services_home.jpg'
        ],
        [
            'type' => 'img',
            'value' => 'bg_team_home.jpg'
        ],
        [
            'type' => 'img',
            'value' => 'bg_about_home.jpg'
        ],
        [
            'type' => 'video',
            'value' => 'https://www.youtube.com/embed/-337c2a3s0A?rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2F127.0.0.1%3A8000&amp;widgetid=3'
        ],
        [
            'type' => 'prevImg',
            'value' => 'bg_banner_home.jpg'
        ]

    ];
    public function run()
    {
        DB::table('medias')->insert($this->medias);
    }
}
