<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $videos = [
        [
            'video_link' =>'https://youtu.be/-337c2a3s0A',
            'video_id' =>'mkHY5-r_B9M',
        ],
        [
            'video_link' =>'https://youtu.be/-337c2a3s0A',
            'video_id' =>'mkHY5-r_B9M',
        ],
        [
            'video_link' =>'https://youtu.be/-337c2a3s0A',
            'video_id' =>'mkHY5-r_B9M',
        ],
        [
            'video_link' =>'https://youtu.be/-337c2a3s0A',
            'video_id' =>'mkHY5-r_B9M',
        ],
        [
            'video_link' =>'https://youtu.be/-337c2a3s0A',
            'video_id' =>'mkHY5-r_B9M',
        ],
    ];
    public function run()
    {
        DB::table('videos')->insert($this->videos);
    }
}
