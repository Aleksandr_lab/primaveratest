<?php

use Illuminate\Database\Seeder;

class UsersPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $users_permissions = [
        [
            'user_id' => 1,
            'permission_id' => 1,
        ],
        [
            'user_id' => 1,
            'permission_id' => 2,
        ],
        [
            'user_id' => 1,
            'permission_id' => 3,
        ],
        [
            'user_id' => 1,
            'permission_id' => 4,
        ],
        [
            'user_id' => 2,
            'permission_id' => 1,
        ],
        [
        'user_id' => 2,
            'permission_id' => 2,
        ],
        [
            'user_id' => 2,
            'permission_id' => 3,
        ]
    ];
    public function run()
    {
        DB::table('users_permissions')->insert($this->users_permissions);
    }
}
