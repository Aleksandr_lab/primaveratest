<?php

use Illuminate\Database\Seeder;

class SectionMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $section_media = [
        [
            'section_id' => 1,
            'media_id' => 1
        ],
        [
            'section_id' => 2,
            'media_id' => 2
        ],
        [
            'section_id' => 3,
            'media_id' => 3
        ],
        [
            'section_id' => 4,
            'media_id' => 4
        ],
        [
            'section_id' => 1,
            'media_id' => 5
        ],
        [
            'section_id' => 2,
            'media_id' => 6
        ],
        [
            'section_id' => 2,
            'media_id' => 7
        ]
    ];
    public function run()
    {
        DB::table('medias_sections')->insert($this->section_media);
    }
}
