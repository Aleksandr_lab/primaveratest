<?php

use Illuminate\Database\Seeder;

class MainSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $settings = [
            [
                'type' => 'logo',
                'name' => 'logo',
                'value' => 'logo.svg'
            ],
            [
                'type' => 'social',
                "name" => "fb",
                "value" => "www.facebook.com"
            ],
            [
                'type' => 'social',
                "name" => "inst",
                "value" => "www.instagram.com"
            ],
            [
                'type' => 'contact',
                "name" => "email",
                "value" => "www.gmail.com"
            ],
            [
                'type' => 'contact',
                "name" => "tel",
                "value" => "+380980420002"
            ],
            [
                'type' => 'contact',
                "name" => "tel",
                "value" => "++380500420002"
            ],
            [
                'type' => 'contact',
                "name" => "tel",
                "value" => "+380612229031"
            ],
            [
                'type' => 'contactAddress',
                "name" => "link",
                "value" => 'https://goo.gl/maps/8CTFZr43ZDMQrmJz6'
            ],
            [
                'type' => 'contactAddress',
                "name" => "name",
                "value" => 'Запорожье, пр. Соборный, 170А'
            ],
            [
                'type' => 'meta',
                'name' => 'keywords',
                'value' => 'keywords'
            ],
            [
                'type' => 'meta',
                'name' => 'description',
                'value' => 'description'
            ]
        ];
    public function run()
    {
        DB::table('main_settings')->insert($this->settings);
    }
}
