<?php

use App\Models\MainSetting;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            PermissionsTableSeesder::class,
            UsersPermissionsTableSeeder::class,
            PostCategoriesSeeder::class,
            ServiceCategorySeeder::class,
            ServiceSubCategoriesTableSeeder::class,
            ServicesSeeder::class,
            PostsTableSeeder::class,
            PhotosTableSeeder::class,
            VideosTableSeeder::class,
            MainSettingsSeeder::class,
            MediasTableSeeder::class,
            PagesTableSeeder::class,
            SectionsTableSeeder::class,
            SectionMediaTableSeeder::class
        ]);
    }
}
