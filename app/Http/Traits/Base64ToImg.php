<?php


namespace App\Http\Traits;


trait Base64ToImg
{
    public function to_img($img_string, $path)
    {
        $dir = opendir($path);
        $count = 0;
        while($file = readdir($dir)){
            if($file == '.' || $file == '..' || is_dir($path . $file)){
                continue;
            }
            $count++;
        }
        $count++;
        $data = explode( ',', $img_string );
        $mime_type = explode('/', $data[0]);
        $mime_type = explode(';', $mime_type[1]);
        $mime_type = $mime_type[0];
        $img_name = 'photo'.$count.'.'.$mime_type;
        $ifp = fopen( $path.$img_name, 'wb' );
        fwrite( $ifp, base64_decode( $data[1] ) );
        fclose( $ifp );

        return $img_name;
    }

    public function unlink_image($img_path)
    {
        unlink($img_path);
    }
}
