<?php


namespace App\Http\Traits;


use Illuminate\Http\Request;

trait UploadSvg
{
    private function validateSvg()
    {

    }

    public function saveSvg(string $imgName, string $svg = '')
    {
        $path = public_path('/resources/img/svg/');
        $ifp = fopen( $path.$imgName, 'wb' );
        fwrite( $ifp, $svg);
        fclose( $ifp );
        return 'svg';
    }
}
