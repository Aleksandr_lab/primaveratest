<?php


namespace App\Http\Traits;


trait DBErrors
{
    public function getDatabaseError(int $numRows)
    {
        if ($numRows === 0){
            return 'Cannot find such row';
        } else {
            return false;
        }
    }
}
