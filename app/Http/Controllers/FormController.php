<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function addSubscriber(Request $request) :string
    {
        $validate_data = $request->validate([
            'email' => 'required|email'
        ]);
        $error = [];
        try {
            Client::create(['email' => $request->email]);
            $error = [
                'error' => false
            ];
        }catch (QueryException $e){
            $error = [
                'error' => true
            ];
        }

        return json_encode($error);
    }

    public function addContactRequest(Request $request) : string
    {
        $validate_data = $request->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $error = [];
        try {
            Client::create([
                'email' => $request->email,
                'name' => $request->name,
                'comments' => $request->message
            ]);
            $error = [
                'error' => false
            ];
        }catch (QueryException $e){
            $error = [
                'error' => true
            ];
        }

        return json_encode($error);
    }
}
