<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServiceSubCategory;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function index()
    {
        $services_categories = [
            'services' => ServiceCategory::with('subcategories.services')->orderBy('sort_order')->get()->toArray()
        ];
        return json_encode($services_categories);
    }

    public function get_services()
    {
        return $services = Service::all()->toJson();
    }
    public function get_service(string $slug) : string
    {
        $response = [
            'service' =>  ServiceCategory::where('slug', $slug)->with('subcategories.services')->orderBy('sort_order')->first()->toArray()
        ];
        return json_encode($response);
    }
    public function get_service_sub_category(string $slug) : string
    {
        $response = [
            'service' =>  ServiceSubCategory::where('slug', $slug)->with('services')->first()->toArray()
        ];
        return json_encode($response);
    }
}
