<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Video;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {

    }

    public function get_photos()
    {
        $result = [
            'gallery_image' => Photo::all()->toArray()
        ];

        return $result;
    }

    public function get_videos()
    {
        $result = [
            'gallery_video' => Video::all()->toArray()
        ];

        return json_encode($result);
    }
}
