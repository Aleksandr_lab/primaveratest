<?php

namespace App\Http\Controllers;

use App\Mail\OrderEmail;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    private function store($request)
    {
        $store_data = [
            'client_name' => $request['name'],
            'client_phone' => '+380'.$request['phone'],
            'client_email' => $request['email'],
            'chosen_service' => $request['service'],
            'additional_info' => $request['info']
        ];
        Order::create($store_data);
    }

    public  function send_mail(Request $request)
    {
        $validate_data = $request->validate([
            'name' => 'required|max:64',
            'phone' => 'required',
            'email' => 'required|email',
            'service' => 'required'
        ]);
        $request->phone = '+380'.$request->phone;
        $this->store($request);
        Mail::to('academws1@gmail.com')->send(new OrderEmail($request));

    }
}
