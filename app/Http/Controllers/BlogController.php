<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\PostsCategory;

class BlogController extends Controller
{
    public function index() : string
    {
        $posts_categories = PostsCategory::all()->toArray();
        $posts = Post::with('category')->get()->toArray();
        $response = [
            'posts_categories' => $posts_categories,
            'posts' => $posts
        ];
        return json_encode($response);
    }

    public function get_posts_by_category(int $category_id) : string
    {
        return Post::where('category_id', $category_id)->get()->toJson();
    }

    public function get_post(string $slug) : string
    {
        return Post::where('slug', $slug)->with('category')->first()->toJson();
    }
}
