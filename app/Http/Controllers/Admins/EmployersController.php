<?php

namespace App\Http\Controllers\Admins;

use App\Models\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'emplPhoto' => 'required',
            'emplName' => 'required',
            'emplSurname' => 'required',
            'emplPosition' => 'required'
        ]);
        $photo = $request->file('emplPhoto');

        if (isset($photo)){
            $photo->move(public_path('resources/img/employers/'), $photo->getClientOriginalName());
            $photo_link = $photo->getClientOriginalName();
        }else{
            $photo_link = '';
        }

        Employer::create([
            'photo' => $photo_link,
            'name' => $request->emplName,
            'surname' => $request->emplSurname,
            'position' => $request->emplPosition,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
