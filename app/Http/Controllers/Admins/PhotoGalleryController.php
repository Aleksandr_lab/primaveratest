<?php

namespace App\Http\Controllers\Admins;

use App\Http\Traits\Base64ToImg;
use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoGalleryController extends Controller
{
    use Base64ToImg;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return Photo::all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_link = $this->to_img($request->image, public_path('/resources/img/'));
        $image_link = public_path('/resources/img/').$image_link;
        Photo::create([
            'alt' => $request->alt,
            'image_link' => $image_link
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        return Photo::where('id', $photo->id)->first()->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        return Photo::where('id', $photo->id)->first()->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        $image_link = $this->to_img($request->image, public_path('/resources/img/'));
        $image_link = public_path('/resources/img/').$image_link;
        Photo::where('id', $photo->id)->update([
            'alt' => $request->alt,
            'image_link' => $image_link,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        $photo = Photo::where('id', $photo->id)->first();
        $this->unlink_image($photo->image_link);
        Photo::where('id', $photo->id)->delete();
    }
}
