<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;

class ServiceCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return ServiceCategory::all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = public_path('/resources/img/serviceCategory/');
        $image = $request->file('image');
        if (isset($image)) {
            $image->move($path, $image->getClientOriginalName());
        }

        $create = ServiceCategory::create([
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'sort_order' => $request->sort_order,
            'image' => $image->getClientOriginalName(),
            'slug' => $request->slug
        ]);

        return $create;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        return ServiceCategory::where('id', $id)->first()->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        return ServiceCategory::where('id', $id)->update([
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'slug' => $request->slug,
            'sort_order' => $request->sort_order,
            'text' => $request->text
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        ServiceCategory::where('id', $id)->delete();
    }
}
