<?php

namespace App\Http\Controllers\Admins;

use App\Models\Service;
use App\Models\ServiceSubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return Service::with('subcategory')->orderBy('sort_order')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() : string
    {
        return ServiceSubCategory::all()->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Service::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'price' => $request->price*100,
            'sort_order' => $request->sort_order,
            'body' => $request->body,
            'subcategory_id' => $request->subcategory_id
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return Service::where('id', $id)->first()->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $service = Service::with('subcategory')->where('id', $id)->first()->toArray();
        $sub_categories = ServiceSubCategory::orderBy('sort_order')->get()->toArray();
        $response = [
            'service' => $service,
            'subcategories' => $sub_categories
        ];
        $response = json_encode($response);
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {

        Service::where('id', $id)->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'price' => $request->price*100,
            'sort_order' => $request->sort_order,
            'body' => $request->body,
            'subcategory_id' => $request->subcategory_id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        Service::where('id', $id)->delete();
    }
}
