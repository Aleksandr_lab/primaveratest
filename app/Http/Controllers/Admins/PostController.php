<?php

namespace App\Http\Controllers\Admins;

use App\Http\Traits\Base64ToImg;
use App\Models\Post;
use App\Models\PostsCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    use Base64ToImg;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return Post::with('category')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_link = $this->to_img($request->image, public_path('/resources/img/'));
        $image_link = public_path('/resources/img/').$image_link;
        Post::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'category_id' => $request->category_id,
            'image_link' => $image_link,
            'body' => $request->body
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return Post::where('id', $post->id)->first()->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $post = Post::with('category')->where('id', $post->id)->first()->toArray();
        $categories = PostsCategory::all()->toArray();

        $response = [
            'post' => $post,
            'categories' => $categories
        ];

        $response = json_encode($response);

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
       /* $image_link = $this->to_img($request->image, public_path('/resources/img/'));
        $image_link = public_path('/resources/img/').$image_link;*/
        Post::where('id', $post->id)->update([
            'title' => $request->title,
            'sub_title' => $request->subTitle,
            'slug' => $request->slug,
            'category_id' => $request->category_id,
            'image_link' => $request->image,
            'body' => $request->body
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        Post::where('id', $post->id)->delete();
    }
}
