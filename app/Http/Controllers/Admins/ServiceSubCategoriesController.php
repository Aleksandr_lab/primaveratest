<?php

namespace App\Http\Controllers\Admins;

use App\Models\ServiceCategory;
use App\Models\ServiceSubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceSubCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() :string
    {
        return ServiceSubCategory::with('category')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() : string
    {
        return ServiceCategory::all()->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ServiceSubCategory::create([
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'category_id' => $request->category,
            'sort_order' => $request->sort_order,
            'slug' => $request->slug,
            'text' => $request->text
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id) : string
    {
        return ServiceSubCategory::where('id', $id)->first()->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        ServiceSubCategory::where('id', $id)->update([
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'slug' => $request->slug,
            'sort_order' => $request->sort_order,
            'text' => $request->text
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        ServiceSubCategory::where('id', $id)->delete();
    }
}
