<?php

namespace App\Http\Controllers\Admins;

use App\Http\Traits\DBErrors;
use App\Http\Traits\UploadSvg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MainSetting;

class MainSettingsController extends Controller
{
    use DBErrors, UploadSvg;
    private $filePath = '/resources/img/svg/';

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getAllSettings()
    {
        return MainSetting::all()->toJson();
    }

    public function updateSetting(Request $request, int $id)
    {
        $logo = $request->file('logo');
        if(isset($logo)){
            $logo->move(public_path($this->filePath), $logo->getClientOriginalName());
        }

        /*if(isset($logo)){
            if ($logo->getClientOriginalExtension() === 'svg' && $logo->getClientMimeType() === 'image/svg+xml'){
                $logo->move(public_path($this->filePath), $logo->getClientOriginalName());
            }else{
                $error = [
                    'status' => false,
                    'message' => "Логотип должен быть в формате svg"
                ];
                return $error;
            }
        }*/
        $res = MainSetting::where('id', $id)->update([
            'value' => $request->value
        ]);
        if ($this->getDatabaseError($res)){
            $error = [
                'status' => false
            ];
            return json_encode($error);
        }
    }

    public function createContact(Request $request)
    {
        $validateData = $request->validate([
            'newContact' => 'required',
            'type' => 'required'
        ]);

        MainSetting::create([
            'type' => 'contact',
            'name' => $request->type,
            'value' => $request->newContact
        ]);
    }
    public function deleteSetting(int $id)
    {
        MainSetting::where('id', $id)->delete();
    }
}
