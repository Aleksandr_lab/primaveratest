<?php

namespace App\Http\Controllers\Admins;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientsController extends Controller
{

    public function getSubscribers()
    {
        return Client::whereNull('name')->get()->toJson();
    }

    public function getSubmitters()
    {
        return Client::whereNotNull('name')->get()->toJson();
    }
}
