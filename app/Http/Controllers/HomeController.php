<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\MainSetting;
use App\Models\PostsCategory;
use App\Models\Section;
use App\Models\ServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class HomeController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function getMainSettings() : string
    {
        $settings = MainSetting::all();

        $mainSettings = array();
        foreach ($settings as $setting){
            switch ($setting->type){
                case 'social' :
                    $mainSettings['socials'][$setting->name] = $setting->value;
                    break;
                case 'contact' :
                    if ($setting->name === 'tel'){
                        $mainSettings['contactPhones'][] = $setting->value;
                    } else {
                        $mainSettings['contactEmail'] = $setting->value;
                    }
                    break;
                case 'logo' :
                    $mainSettings['logo'] = $setting->value;
                    break;
                case 'contactAddress' :
                    $mainSettings['contactAddress'][$setting->name] = $setting->value;
                    break;
                default :
                    $mainSettings['others'][$setting->name] = $setting->value;
            }
        }

        return json_encode(['settings'=>$mainSettings]);
    }

    public function getBannerSection() : string {
        $banner = Section::where('slug', 'banner')->with('medias')->first();
        $img = '';
        $video = '';
        foreach ($banner->medias as $media){
            switch ($media->type) {
                case 'img' :
                    $img = $media->value;
                    break;
                case 'video' :
                    $video = $media->value;
                    break;
                default :
                    $other_media[] = $media;
                    break;
            }
        }
        $result = [
            'banner' => [
                'image' => $img,
                'video' => $video,
            ]
        ];
        return json_encode($result);
    }

    public function getAboutSection() : string
    {
        $about = Section::where('slug', 'about')->with('medias')->first();
        $img = '';
        $video = '';
        $other_media = [];
        foreach ($about->medias as $media){
            switch ($media->type) {
                case 'img' :
                    $img = $media;
                    break;
                case 'video' :
                    $video = $media;
                    break;
                default :
                    $other_media[] = $media;
                    break;
            }
        }
        $result = [
            'about' => [
                'id' => $about->id,
                'title' => $about->title,
                'subtitle' => $about->subtitle,
                'image' => $img,
                'video' => $video,
                'text' => $about->text
            ]
        ];
        return json_encode($result);
    }

    public function getServicesSection() : string
    {
        $mpText = Section::where('slug', 'services')->first()->toArray();
        $mpServices = ServiceCategory::select('id', 'title', 'image', 'slug')->orderBy('sort_order', 'asc')->get()->toArray();
        $result = [
            'text' => $mpText,
            'services' => $mpServices
        ];

        return json_encode($result);

    }

    public function getImportantSection() : string
    {
        $result = [
            'important' => Section::where('slug', 'important')->first()->toArray()
        ];

        return json_encode($result);
    }

    public function getTeamSection() : string
    {
        $result = [
            'team' => Section::where('slug', 'team')->first()->toArray()
        ];

        return json_encode($result);
    }
    public function getNewsSection() : string
    {
        $post_categories = PostsCategory::all();
        $result = [];
        foreach($post_categories as $category){
            $result[]= PostsCategory::where('id', $category->id)->with('mainPagePosts')->first()->toArray();
        }

        $result = [
            'news' => Section::where('slug', 'news')->first()->toArray(),
            'categoriesWithPosts' => $result
        ];

        return json_encode($result);
    }

    public function getPartnersSection() : string
    {
        return Section::where('slug', 'partners')->with('medias')->first()->toJson();
    }
}
