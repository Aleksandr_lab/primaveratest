<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    //private static $admin_role = 'admin';

    public function handle($request, Closure $next, $role)
    {
        if (! $request->user()->hasRole($role)){
            return redirect()->route('/');
        }
        return $next($request);
    }
}
