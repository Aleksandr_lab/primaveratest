<?php


namespace App\Http\Helpers\mpSections;


use App\Models\Section;

class MpSection
{
    private $section = null;
    protected $path = '/resources/img/homePage/';

    public function getSection(int $id)
    {
        $this->section = Section::where('id', $id)->with('medias')->first();
        $class = __NAMESPACE__.'\Section'.ucfirst($this->section->slug);
        $obj = new $class;
        return $obj;
    }

    public function getMedia()
    {
        return $this->section->medias;
    }
}
