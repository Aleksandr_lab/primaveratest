<?php

namespace App\Http\Helpers\mpSections;

use App\Models\Section;
use Illuminate\Http\Request;

class SectionNews extends MpSection
{
    public function updateSection(Request $request, int $id = 0)
    {
        $res = Section::where('id', $id)->update([
            'title' => $request->title,
            'text' => $request->text,
        ]);
        return 1;
    }
}
