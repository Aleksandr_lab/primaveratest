<?php


namespace App\Http\Helpers\mpSections;


use App\Models\Media;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionAbout extends MpSection
{
    public function updateSection(Request $request, int $id = 0)
    {
        $path = public_path($this->path);
        $image = $request->file('image');
        if (isset($image)) {
            $image->move($path, $image->getClientOriginalName());

            $media = Media::where('id', $request->imageId)->update([
                'value' => $image->getClientOriginalName()
            ]);
        }
        $video = Media::where('id', $request->videoID)->update([
            'value' => $request->videoLink
        ]);
        $res = Section::where('id', $id)->update([
            'title' => $request->title,
            'sub_title' => $request->subTitle,
            'text' => $request->text,
        ]);
        return 1;
    }
}
