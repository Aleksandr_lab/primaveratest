<?php


namespace App\Http\Helpers\abSections;

use App\Models\Media;
use Illuminate\Http\Request;

class SectionAbBanner extends AbSection
{
    public function updateSection(Request $request, int $id = 0)
    {
        dd($request);
        $path = public_path($this->path);
        $image = $request->file('bannerImage');
        $res = 0;
        if (isset($image)) {
            $image->move($path, $image->getClientOriginalName());

            $res = Media::where('id', $request->mediaId)->update([
                'value' => $image->getClientOriginalName()
            ]);
        }
        if ($res === 1){
            $errors = [
                'error' => false
            ];
        }
        if ($res === 0){
            $errors = [
                'error' => true
            ];
        }
        return json_encode($errors);
    }
}
