<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainPageSection extends Model
{
    protected $fillable = ['sort_order', 'section_id'];

    public function sections()
    {
        return $this->hasMany('App\Models\Section', 'section_id', 'id');
    }
}
