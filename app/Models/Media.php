<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['type', 'url'];
    public $table = 'medias';

    public function sections()
    {
        return $this->belongsToMany('App\Models\Section', 'media_section', 'media_id', 'section_id');
    }
}
