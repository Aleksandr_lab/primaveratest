<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\ServiceCategory;
use App\Models\Service;

class ServiceSubCategory extends Model
{
    protected $fillable = ['title', 'slug', 'sort_order', 'category_id'];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function category()
    {
        return $this->belongsTo(ServiceCategory::class, 'category_id');
    }

    public function services()
    {
        return $this->hasMany(Service::class, 'subcategory_id', 'id')->orderBy('price');
    }
}
