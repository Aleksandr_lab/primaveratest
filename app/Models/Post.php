<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PostsCategory;
class Post extends Model
{
    protected $fillable = ['slug', 'name', 'category_id','image_link', 'body'];

    public function category()
    {
        return $this->belongsTo(PostsCategory::class, 'category_id');
    }
}
