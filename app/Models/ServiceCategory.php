<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\ServiceSubCategory;
use App\Models\Service;

class ServiceCategory extends Model
{
    protected $fillable = ['title', 'slug', 'sort_order', 'image'];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function subcategories()
    {
        return $this->hasMany(ServiceSubCategory::class, 'category_id', 'id');
    }

    public function services()
    {
        return $this->hasMany(Service::class, 'sub_category_id');
    }

    /*public function services()
    {
        return$this->hasManyThrough(
                 Service::class,
                ServiceSubCategory::class,
                'category_id',
             'subcategory_id',
                'id',
            'id'
            );
    }*/
}
