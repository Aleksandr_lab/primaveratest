<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Permission extends Model
{
    protected $fillable = ['slug', 'name'];

    public function user()
    {
        return$this->belongsToMany(User::class, 'user_permissions');
    }
}
