<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
class PostsCategory extends Model
{
    protected $table = 'post_categories';

    protected $fillable = ['slug', 'title'];

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }

    public function mainPagePosts()
    {
        return $this->hasMany(Post::class, 'category_id', 'id')
            ->orderBy('created_at', 'desc')
            ->limit(4);
    }
}
