<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ServiceSubCategory;
use App\Models\ServiceCategory;

class Service extends Model
{
    protected $fillable = ['title', 'slug', 'body', 'sort_order', 'price'];

    public function subcategory()
    {
        return $this->belongsTo(ServiceSubCategory::class, 'subcategory_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(ServiceCategory::class);
    }
}
