<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['title', 'sub_title', 'text'];

    public function mainPage()
    {
        return $this->belongsTo('App\Models\MainPageSection', 'section_id');
    }

    public function medias()
    {
        return $this->belongsToMany('App\Models\Media', 'medias_sections', 'section_id', 'media_id');
    }

}
