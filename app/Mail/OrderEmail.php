<?php

namespace App\Mail;

use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name ='';
    private $phone = '';
    private $email = '';
    private $service = '';
    private $info = '';

    public function __construct($request)
    {
        $this->name = $request->name;
        $this->phone = $request->phone;
        $this->email = $request->email;
        $this->service = $request->service;
        $this->info = $request->info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('wizzard.zp@gmail.com')
                    ->view('emails.order')
                    ->with([
                        'client_name' => $this->name,
                        'client_phone' => $this->phone,
                        'client_email' => $this->email,
                        'chosen_service' => $this->service,
                        'additional_info' => $this->info
                    ]);
    }
}
