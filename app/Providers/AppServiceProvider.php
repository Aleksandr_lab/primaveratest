<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Photo;
//use Illuminate\View\View;
use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $photos = Photo::select('image_link')->get();
//        View::share('photos', $photos);

    }
}
